#include "esphome.h"
#include <Adafruit_CCS811.h>

using namespace esphome;

class MySensors : public PollingComponent {
 public:

  Adafruit_CCS811 co2 = Adafruit_CCS811();

  sensor::Sensor co2_sensor = sensor::Sensor();
  sensor::Sensor tvoc_sensor = sensor::Sensor();

  // constructor
  MySensors() : PollingComponent(60000) {}

  void setup() override {
    bool status = co2.begin();
    while(!co2.available());
    float temp = co2.calculateTemperature();
    co2.setTempOffset(temp - 25.0);

  }

  void update() override {
    co2.readData();
    co2_sensor.publish_state(co2.geteCO2());
    tvoc_sensor.publish_state(co2.getTVOC());
  }
};
